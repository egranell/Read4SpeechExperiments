# Read4SpeechExperiments
Read4SpeechExperiments allows obtaining voice samples for experiments in
automatic speech recognition.

The set of sentences to read can be showed to the speakers by text or by images.
Speakers can send the recorded speech utterances by using any communication
application available on their mobile device, such as, e-mail. Moreover, this
tool includes a handsfree mode for acquiring speech utterances with a reduced
speaker interaction.
